﻿using System.Collections.Generic;
using DryIoc;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Logger;
using DotStat.Db.Repository.SqlServer;
using Microsoft.Extensions.Configuration;

namespace DotStatServices.AuthorizationManagement
{

    [ExcludeFromCodeCoverage]
    internal class ApiIoc
    {
        public ApiIoc(IContainer container, IConfiguration configuration)
        {
            RegisterConfiguration(container, configuration);
            RegisterServices(container);
        }

        private static void RegisterConfiguration(IContainer container, IConfiguration configuration)
        {
            var authConfig = configuration.GetSection("auth").Get<AuthConfiguration>() ?? AuthConfiguration.Default;
            var baseConfig = configuration.Get<BaseConfiguration>();

            container.RegisterInstance(baseConfig);
            container.RegisterInstance<ILocalizationConfiguration>(baseConfig);
            container.RegisterInstance<IGeneralConfiguration>(baseConfig);
            container.RegisterInstance<IAuthConfiguration>(authConfig);

            // Log4Net
            LogHelper.ConfigureAppenders(baseConfig);
        }

        private static void RegisterServices(IContainer container)
        {
            container.Register<IAuthorizationRepository, SqlAuthorizationRepository>(Reuse.Singleton);
            container.Register<IAuthorizationManagement, DotStat.Common.Auth.AuthorizationManagement>(Reuse.Singleton);
        }
    }
}