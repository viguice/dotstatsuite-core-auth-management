﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DotStatServices.AuthorizationManagement
{
    [ExcludeFromCodeCoverage]
    public sealed class JsonExceptionMiddleware
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(JsonExceptionMiddleware));
        private static readonly JsonSerializer Serializer;

        static JsonExceptionMiddleware()
        {
            Serializer = new JsonSerializer()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        public static async Task Invoke(HttpContext context)
        {
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            context.Response.ContentType = "application/json";

            var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;

            if (ex == null)
                return;

            Log.Error("Unhandled error", ex);

            var error = BuildError(ex);

            await using var writer = new StreamWriter(context.Response.Body);

            Serializer.Serialize(writer, error);
            
            await writer.FlushAsync().ConfigureAwait(false);
        }

        private static OperationResult BuildError(Exception ex)
        {
            return OperationResult.Error(ex.Message);
        }
    }
}
